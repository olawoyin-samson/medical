$(".mint").on("click",function(){
  
})


function filterTable(table,column,data_){
  var table_ = $(table).DataTable();
  var t = table_.data();

  $.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
      var transaction_colum = data[column] ; // use data for the age colum
      if (data_ == transaction_colum) {
        return true;
      }

      return false;

    }
  );

  table_.draw(); 
}

$("#forget-pwd").on("click",function(e){
  e.preventDefault();
  $("#signin-form").addClass("off-display");
  $("#reset-pwd-container").removeClass("off-display");

})

$("#visit-btn").on("click",function(){
  $("#modal_visit").modal('toggle');
})

$(".btn_view_visit").on("click",function(){
  $(".modal_view_visit").modal('toggle');
  var id = $(this).attr("key");

  $.ajax({
    type : "get",
    url  : "/viewAppointment/"+id,
    contentType: "application/json",
    success : function(data){
      if(data.Amount != null){
        $("#visit-amount").html(data.Amount);
      }

      if(data.DoctorRemark != null){
        $("#visit-doc-remark").html(data.DoctorRemark);
      }

      if(data.Doctor != null){
        $("#visit-doctor").html(data.Doctor);
      } 
      
      $("#visit-date").html(data.Date);

      if(data.Time2 != undefined){
        $("#visit-time").html(data.Time +" - "+ data.Time2);
      }else{
        $("#visit-time").html(data.Time);
      }
      
      $("#visit-message").html(data.Message);

    }
  })
})

$(".view_appointment").on("click",function(){
  $(".modal_view_appointment").modal('toggle');
})

$(".update_doctor").on("click",function(){
  $("#modal_update_doctor").modal("toggle");
})

$("#visit-form").on("submit",function(e){
  alert("Ready to submit form")
  e.preventDefault();
  var f = document.getElementById("visit-form");
  var form = new FormData(f);

  $.ajax({
    type : "post",
    url  :  "/visit-form",
    data : form,
    processData : false,
    contentType : false,
    success : function(){
      $("#modal_visit").modal("toggle");
      $("#modal-notification-message").html("Data successfully saved");
      $("#modal-notification").modal("toggle");
    }

  })
})

$(".view_admin_visit").on("click",function(){
  $("#admin_view_visit_modal").modal('toggle');
  var id = $(this).attr("key");

  $.ajax({
    type : "get",
    url  : "/viewAppointment/"+id,
    contentType: "application/json",
    success : function(data){
      if(data.Amount != null){
        $("#visit-amount").html(data.Amount);
      }

      if(data.DoctorRemark != null){
        $("#visit-doc-remark").html(data.DoctorRemark);
      }

      if(data.Doctor != null){
        $("#visit-doctor").html(data.Doctor);
      } 
      
      $("#visit-date").html(data.Date);

      if(data.Time2 != undefined){
        $("#visit-time").html(data.Time +" - "+ data.Time2);
      }else{
        $("#visit-time").html(data.Time);
      }
      
      $("#visit-message").html(data.Message);
      $("#visit-patient").html(data.Email);
      $("#visit-id").attr("value",data._id);
      $("#visit-amount").attr("value",data.Amount);
      $("#visit-remark").attr("value",data.DoctorRemark);
    }
  })
})

$("#pass_login_btn").on("click",function(){
  $("#pass_landing_page").addClass("off-display");
  $("#pass_login_form").removeClass("off-display");
  $("#signup-form").addClass("off-display");
  $("#signin-form").removeClass("off-display");
})

$("#pass_register_btn").on("click",function(){
  $("#pass_landing_page").addClass("off-display");
  $("#pass_login_form").removeClass("off-display");
  $("#signup-form").removeClass("off-display");
  $("#signin-form").addClass("off-display");
})

$("#back_login_btn").on("click",function(){
  $("#signup-form").addClass("off-display");
  $("#signin-form").removeClass("off-display"); 
  $("#reset-pwd-container").addClass("off-display"); 
})


function updatePatientDetails(){
  $.ajax({
    type : "get",
    url : "patient-details",
    success : function(data){
      var data = data[0];
      console.log(data);
      setAttr("#patient-fname",data.FirstName);
      setAttr("#patient-lname",data.LastName);
      setAttr("#patient-phone",data.Phone);
      $("#patient-pid").html(data.PatientId);
      setAttr("#patient-id",data._id);
    }
  })
}

function setAttr(id,val){
  $(""+id).attr("value",val);
}

updatePatientDetails();

$(".update_doctor_").on("click",function(){
  $("#modal_update_doctor").modal("toggle");
  var id = $(this).attr("key");

  $.ajax({
    type : "get",
    url : "get-doctor/"+id,
    success :  function(data){
      console.log(data);
      setAttr("#doctor-fname",data[0].FirstName);
      setAttr("#doctor-lname",data[0].LastName);
    }
  })
})

$("#settings").on("click",function(){
  $("#modal-settings").modal("toggle");

  $.ajax({
    type : "get",
    url : "mail-settings",
    success : function(data){
      var data = data[0];
      setAttr("#sett-id",data._id);
      setAttr("#sett-smtp",data.smtp);
      setAttr("#sett-username",data.username);
      setAttr("#sett-password",data.password);  
      setAttr("#sett-port",data.port);     
    }
  })
})

function closeModal(id){
  $(id).modal('toggle');
}

$("#dob-picker").datepicker({changeYear: true,changeMonth: true,yearRange: "-90:+0",});
