
    const checkboxes = 'input[type=checkbox]',
          radios = 'input[type=radio]'; 
    
    // initialization    
    lc_switch(checkboxes);
    lc_switch(radios, {
        compact_mode : true,
        on_color : 'teal',
    }); 
        
    
    
    // enable/disable
    document.getElementById('enable_ckb').addEventListener('click', () => { lcs_enable(checkboxes); });    
    document.getElementById('disable_ckb').addEventListener('click', () => { lcs_disable(checkboxes); });    
        
    document.getElementById('enable_radio').addEventListener('click', () => { lcs_enable(radios); });    
    document.getElementById('disable_radio').addEventListener('click', () => { lcs_disable(radios); });        
        
        
        
    // events log
    const log_wrap = document.querySelector('#third_div ul');
       
    const toLeadingZero = (val) => {
        return (val.toString().length < 2) ? '0'+val : val;    
    };
        
    const getTime = () => {
        const d = new Date;
        return '('+ toLeadingZero(d.getHours()) +':'+ toLeadingZero(d.getMinutes()) +':'+ toLeadingZero(d.getSeconds()) +')';
    };
        
    document.querySelectorAll('input').forEach(function(el) {
        const subj = el.getAttribute('type') +' #'+ el.value; 
        
        el.addEventListener('lcs-statuschange', (e) => {
            let status = (el.checked) ? 'checked' : 'unchecked';
            status += (el.disabled) ? ' disabled' : ' enabled';
            
            log_wrap.innerHTML = '<li><em>'+ getTime() +' [lcs-statuschange]</em>'+ subj +' changed status: '+ status +'</li>' + log_wrap.innerHTML;     
        });
        
        el.addEventListener('lcs-on', (e) => {
            const status = (el.checked) ? 'checked' : 'unchecked';
            log_wrap.innerHTML = '<li><em>'+ getTime() +' [lcs-on]</em>'+ subj +' changed status: '+ status +'</li>' + log_wrap.innerHTML;     
        });
        
        el.addEventListener('lcs-off', (e) => {
            const status = (el.checked) ? 'checked' : 'unchecked';
            log_wrap.innerHTML = '<li><em>'+ getTime() +' [lcs-off]</em>'+ subj +' changed status: '+ status +'</li>' + log_wrap.innerHTML;     
        });
        
        el.addEventListener('lcs-enabled', (e) => {
            const status = (el.disabled) ? 'disabled' : 'enabled';
            log_wrap.innerHTML = '<li><em>'+ getTime() +' [lcs-enabled]</em>'+ subj +' changed status: '+ status +'</li>' + log_wrap.innerHTML;     
        });
        
        el.addEventListener('lcs-disabled', (e) => {
            const status = (el.disabled) ? 'disabled' : 'enabled';
            log_wrap.innerHTML = '<li><em>'+ getTime() +' [lcs-disabled]</em>'+ subj +' changed status: '+ status +'</li>' + log_wrap.innerHTML;     
        });
    });
        
    
    // clear events log
    document.querySelector('#third_div small').addEventListener('click', () => {
        log_wrap.innerHTML = '';        
    });
