var mongoose = require('mongoose');
var doctorSchema = mongoose.Schema({
  FirstName : {
    type : String,
    required : true
  },
  LastName : {
    type : String,
    required : true
  },
  Specialization : {
    type : String,
    required : true
  },
  Profile : {
    type : String,
    required : true
  }
});

var doctor = module.exports = mongoose.model("doctor",doctorSchema);