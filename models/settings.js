var mongoose  = require('mongoose');
const settingsSchema = mongoose.Schema({
  port : {
    type : Number,
    required : true
  },
  smtp : {
    type : String,
    required : true
  },
  username : {
    type : String,
    required : true
  }, 
  password : {
    type : String,
    required : false 
  }
});

const settings = module.exports = mongoose.model('settings',settingsSchema);