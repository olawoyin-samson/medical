var mongoose = require('mongoose')

var visitSchema = mongoose.Schema({
  Email : {
    type : String,
    required : true
  },

  UserId : {
    type : String,
    required : true
  },

  Physician : {
    type : String,
    required : true
  },

  Service : {
    type : String,
    required : true
  },

  Message : {
    type : String,
    required : false
  },

  Date : {
    type : Date,
    required : true
  },

  Time : {
    type : String,
    required : true
  },
  Time2 : {
    type : String,
    required : true
  },  

  Doctor : {
    type : String,
    required : false
  },

  DoctorRemark : {
    type : String,
    required : false
  },

  Amount : {
    type : Number,
    required : false
  },

  Status : {
    type : String,
    required : true
  },

  MailCounter : {
    type : Number,
    require : false
  }

})

var visit = module.exports = mongoose.model('visit',visitSchema);