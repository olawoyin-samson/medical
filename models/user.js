var mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

var userSchema = mongoose.Schema({
  username : {
    type : String,
    required  : true
  },
  password : {
    type :String,
    required : true
  },
  role : {
    type : String,
    required : true
  }
})

userSchema.plugin(passportLocalMongoose);
var user = module.exports = mongoose.model('user',userSchema,'user');