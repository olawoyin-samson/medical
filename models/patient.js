var mongoose = require('mongoose');

var patientSchema = mongoose.Schema({
  UserId : {
    type : String,
    required : true
  },
  FirstName : {
    type : String,
    required  : true
  },
  LastName : {
    type : String,
    required  : true
  },
  Phone : {
    type : Number,
    required  : true
  }, 
  Gender : {
    type : String,
    required : true
  },
  PatientId : {
    type : String,
    required : true
  },
  Age : {
    type : Number,
    required : false
  } ,
  DOB : {
    type : Date,
    required  : true
  } 
})

var patient = module.exports = mongoose.model('patient',patientSchema);