var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session =  require('express-session')
var flash = require('connect-flash');
var passport = require('passport');
var User = require('./models/user');
var pages = require('./routes/pages');
var adminPages = require('./routes/admin_pages');
const cron = require("node-cron");
var Visit = require('./models/visit');
var User = require('./models/user');
var nodemailer  = require('nodemailer');

var app = express(); 

// Connecting to mongoDb Container Server
var mongoose = require('mongoose');
const visit = require('./models/visit');
const patient = require('./models/patient');
const { time } = require('console');
var MONGO_URL = "mongodb+srv://root:1234@ostechnologies.pski4.mongodb.net/medical?retryWrites=true&w=majority";
//var MONGO_URL = "mongodb://localhost:27017/medical";
mongoose.connect(MONGO_URL,{useNewUrlParser : true});
var db = mongoose.connection;
db.on('error',console.error.bind(console,'CONNECTION ERROR'));
db.once('open', function(){
  console.log('Connected'); 
})



// Session Middle ware
app.use(session({
  secret : 'keybaord cat',
  resave : false,
  saveUninitialized : true,
  cookie : { maxAge: 60 * 60 * 1000 }
}))

//Express Messages Mildle Ware
app.use(flash());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  res.locals.user = req.user;
  next();
});



//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// Linking the config file
require("./config/passport")(passport);

//passport.use(User.createStrategy());

// To use with session
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', pages);
app.use('/admin/pages',adminPages);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //next(createError(404));
});


// Date compare function
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function sendMail(mailData) {
  let mailTransporter = nodemailer.createTransport({
      port: "465",
      host : "smtp.zoho.com",
      auth: {
        user: "omedicalapp@zohomail.com",
        pass: "1Q2W3E4R5T@2022"
      }
  });
    
  // Setting credentials
  let mailDetails = {
      from: "omedicalapp@zohomail.com",
      to: mailData.To,
      subject: mailData.Subject,
      text: mailData.Body
  };
    
    
  // Sending Email
  mailTransporter.sendMail(mailDetails, 
                  function(err, data) {
      if (err) {
          console.log("Error Occurs", err);
      } else {
          console.log("Email sent successfully to"+ mailData.To);
      } 
  });
};

// Send mail notication
function resendNotification(){
  Visit.find(function(err,visit){
    if(err) console.log(err)
    for( const item of visit){
      var vDate = new Date(item.Date);
      var nDate = new Date();
      var dF = dateDiffInDays(nDate,vDate);
      if(dF === 1 || dF === 0){
        if(item.MailCounter < 8){
          var data = {
            To  : item.Email,
            Subject : "PASS Hospital Schedule for "+item.Date+", Time : "+item.Time+ " - "+item.Time2,
            Body    : "Hello "+item.Email + " <br> this is to notify you that your schedule is for "+item.Date+", From PASS Medical App "+
                      " Time : "+item.Time+" - "+item.Time2 +" Please click ====> http://passapp.com.ng  to sign in into your PASS account."}
            sendMail(data);
          Visit.updateOne({_id : item._id},{$inc : {MailCounter : 1}},function(err){
            if(err) console.log(err) 
          });
        }
      }
    }
  })  
}

// Cron job scheduler
cron.schedule('* 59 * * * *',resendNotification);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(req);
  // render the error page
  res.status(err.status || 500);
  res.render('error',{ref_link : ""});
});



module.exports = app;
