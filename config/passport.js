var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

module.exports = function(passport){
  passport.use(new LocalStrategy(
    function(username,password, done){
    User.find({$and : [{username : username},{password : password}]},function(err,user){
      if (err) console.log(err)

      if(user.length === 0){
        return done(null,false,{message : 'Invalid login using '+username+', please try again'});
      } else {
        return done(null,user);
      }

    });
  }));

  passport.serializeUser(function(user,done){
    done(null ,user[0]._id);
  });

  passport.deserializeUser(function(_id, done){  
    User.findById(_id,function(err,user){
      done(err,user)
    });
  });
}