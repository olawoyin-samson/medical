var express = require('express');
const passport = require('passport');
var router = express.Router();
var User  = require('../models/user');
var Patient  = require('../models/patient');
var Visit = require('../models/visit');
const connectEnsureLogin = require('connect-ensure-login');
var nodemailer = require('nodemailer');


router.get('/login',function(req,res){
  if(req.user != undefined) res.redirect(301,'/appointment');
  res.render('login',{title : "Signup Page"})
})

router.get('/appointment',connectEnsureLogin.ensureLoggedIn(), async function(req,res){
  Visit.find({UserId : req.user._id},function(err,visits){
    if(err) console.log(err)
    res.render('appointment',{title : "Patient Appointment Page",data : visits});
  })  
})

router.get('/patient-details',connectEnsureLogin.ensureLoggedIn(),function(req,res){
  Patient.find({UserId : req.user._id}, function(err,patient){
    if(err) console.log(err)
    res.send(patient); 
  })
})

router.get('/',function(req,res){

  if(res.locals.user){
    switch(res.locals.user.Role){
      case "Patient":
        res.redirect("/appointment");
        break;
        case "Admin":
          res.redirect("admin/pages/home");
          break;
    }
  }else {
    res.render('login',{title : 'Login Page'});
  }  
  
})

router.post('/register-form', function(req,res){
var form = req.body;

 var users = User.findOne({"username" : form.Email},function(err,user){
  if(user){
    req.flash('danger','A User with email <b>'+ form.Email + '</b> already exist,please try again');
    res.render('login',{title : 'Login Page'})
  } else{
    var user = new User(
      {
        username : form.Email,
        password : form.Password,
        role  : "Patient"
      });

    user.save(function(err,user){
      if (err) throw err
    })

    var patient = new Patient(
      {
        UserId : user._id,
        FirstName : form.FirstName,
        LastName  : form.LastName,
        Gender    : form.Gender,
        Phone   : form.Phone,
        PatientId : form.PatientId,
        Age  : null,
        DOB : form.DOB
      });    

      patient.save(function(err){
        if (err) throw err
        req.flash('success',"User Signup successful, please click on the Signin button to access your account.");
        res.render('login',{title : "Login Page"});
        var rData = {To : form.Email,Subject : "PASS Account Sign up", Body : "Account sign up successful, PASS Medical Admin."};
        sendMail(rData);
      })      
  }
 });


})

router.post('/login-form',passport.authenticate('local',{failureRedirect: '/login', failureFlash : true }),function(req,res,next){ 
  
  switch(req.user[0].role){
    case "Patient":
      res.redirect(301,'/appointment');
    break;

    case "Admin":
      res.redirect(301,'admin/pages/home');
    break;
  }  
})

router.get("/logout",function(req,res){
  req.logout();
  res.redirect('/login');
})

router.post("/visit-form", async function(req,res){
  var form  = req.body; 
  var visit = new Visit({
    Email : req.user.username,
    UserId: req.user._id,
    Physician : form.Physician, 
    Service : form.Services,
    Message : form.Message,
    Date  : form.Date,
    Time   : form.Time,
    Time2   : form.Time2,
    Status : "Pending",
    Amount  : null,
    Doctor : null,
    DoctorRemark : null,
    MailCounter : 0
  });

  visit.save(function(err){ 
    if (err) console.log(err);
  });

  req.flash('success',"Visit schedule successfully created.");
  res.redirect(req.get('referer')); 
  var rData = {To : req.user.username,Subject : "PASS Hospital Schedule : Clinic Option : "+form.Physician+" , Date : "+form.Date+", Time : "+form.Time+" - "+form.Time2, Body : "PASS visit schedule setup. Please click ====> http://passapp.com.ng  to sign in into your PASS account.  PASS Admin."}; 
  sendMail(rData);
})

router.get("/viewAppointment/:id",function(req,res){
  var id = req.params.id;
  var visit = Visit.findById(id,function(err,visit){
    if(err) console.log(err)
    res.send(visit);
  });
})

router.post('/reset-pwd-form',async function(req,res){
  var form = req.body;
  if(form.username === "admin@medical.com"){
    req.flash('danger','Admin user account can not be reset.');
    res.redirect(req.get('referer'));
  } else{
    User.findOne({username : form.username},function(err,user){
      if(err) console.log(err)
      if(user != null){
        Patient.find({$and : [{PatientId : form.PatientId},{UserId : user._id}]}, function(err,patient){
          if(err) console.log(err)
          if(patient.length != 0){
            req.flash('success','Password reset successful,====== YOUR PASSWORD IS ====== \n '+user.password);
            res.redirect(req.get('referer'));            
          } else{
            req.flash('danger','Invalid patient ID, please try again.');
            res.redirect(req.get('referer'));            
          }
        })
      } else{
        req.flash('danger','Invalid username, please try again.');
        res.redirect(req.get('referer'));  
      }
    })
  }
})

router.post("/update-patient",async function(req,res){
  var form = req.body;
   Patient.findByIdAndUpdate(form._id,{FirstName : form.FirstName,LastName : form.LastName,Phone : form.Phone},function(err){
     if(err) console.log(err)
     req.flash('success','Profile successfully updated');
     res.redirect(req.get('referer'));
   })
})

function sendMail(mailData) {
  let mailTransporter = nodemailer.createTransport({
      port: "465",
      host : "smtp.zoho.com",
      auth: {
        user: "omedicalapp@zohomail.com",
        pass: "1Q2W3E4R5T@2022"
      }
  });
    
  // Setting credentials
  let mailDetails = {
      from: "omedicalapp@zohomail.com",
      to: mailData.To,
      subject: mailData.Subject,
      text: mailData.Body
  };
    
    
  // Sending Email
  mailTransporter.sendMail(mailDetails, 
                  function(err, data) {
      if (err) {
          console.log("Error Occurs", err);
      } else {
          console.log("Email sent successfully to"+ mailData.To);
      }
  });
};

module.exports = router;