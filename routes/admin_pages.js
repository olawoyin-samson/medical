var express = require('express');
var router  = express.Router();
const connectEnsureLogin = require('connect-ensure-login');
var Visit = require('../models/visit');
const Doctor = require('../models/doctor'); 
const Settings = require('../models/settings');


router.get("/home",connectEnsureLogin.ensureLoggedIn(),async function(req,res){
  Visit.find(function(err,visit){
        if(err) console.log(err)

        Doctor.find(function(err,doctor){
          if(err) console.log(err)
          res.render('admin/pages/home',{title : "Admin Home Page", data : visit , doctor : doctor});
        })
      });
})

router.get("/doctor",connectEnsureLogin.ensureLoggedIn(), async function(req,res){
  Doctor.find(function(err,doctor){
    if(err) console.log(err)
    res.render('admin/pages/doctor',{title : "Doctor Page",data : doctor});
  })
})

router.post("/doctor-form",async function(req,res){
  var form = req.body;
  var doctor = new Doctor({
                            FirstName : form.FirstName,
                            LastName  : form.LastName,
                            Specialization : form.Physician,
                            Profile   : form.Profile
  });

  doctor.save(function(err){
    if(err) console.log(err)
  })

  req.flash("success","data saved successfully");
  res.redirect(req.get('referer'));  
})

router.get('/logout',function(req,res){
  req.logout();
  res.redirect('/login');  
})

router.post('/update-patient-visit',async function(req,res){
  var form = req.body;
  Visit.findByIdAndUpdate(form._id,{Doctor : form.Doctor,Amount : form.Amount,DoctorRemark : form.Remark,Status : "Approved"},function(err){
    if(err) console.log(err);
    req.flash("success","Appointment successfully saved");
    res.redirect(req.get('referer'));
  });
})

router.get("/get-doctor/:id", async function(red,res){
  var id = req.params.id;
  Doctor.findById(id,function(err,doctor){
    if(err) console.log(err)
    res.send(doctor);
  })
})

router.post('/update-mailsettings', function(req,res){
  var form = req.body;
  Settings.findById(form._id,function(err,sett){
    if(err) console.log(err)
    if(sett === null){
      const settings = new Settings({
        port : form.port,
        smtp  : form.smtp,
        username : form.username,
        password : form.password
    
      });

      settings.save(function(err){
        if(err) console.log(err)
      })
      req.flash('success' , "Mail settings successfully created");
      res.redirect(req.get('referer'));
    } else{
      Settings.findByIdAndUpdate(form._id,{
        port : form.port,
        smtp  : form.smtp,
        username : form.username,
        password : form.password
      })
      req.flash('success' , "Mail settings successfully updated");
      res.redirect(req.get('referer'));      
    }
  }) 
})

router.get("/mail-settings", async function(req,res){
  Settings.find(function(err, sett){
    if(err) console.log(err)
    res.send(sett);
  })
})

module.exports = router;